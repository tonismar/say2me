//
//  CloudsightAPI.swift
//  Say2Me
//
//  Created by Tonismar Bernardo on 02/07/17.
//  Copyright © 2017 Tonismar Bernardo. All rights reserved.
//

import Foundation
import CloudSight

class CloudsightAPI: CloudSightQueryDelegate {
    
    var csQuery: CloudSight!
    
    func doDescription(image: Any, withDelegate: sender) {
        
        csQuery = CloudSightQuery(image: UIImageJPEGRepresentation(Any, 0.8), atLocation: CGPoint.zero, withDelegate: sender, atPlacemark: nil, withDeviceId: "device-id")
        
        csQuery.start()
    }
    
    func cloudSightQueryDidFinishUploading(_ query: CloudSightQuery!) {
        print("Call cloudSightQueryDidFinishUploading")
    }
    
    func cloudSightQueryDidFinishIdentifying(_ query: CloudSightQuery!) -> String {
        DispatchQueue.main.async {
            return query.name
        }
    }
    
}
