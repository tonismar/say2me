//
//  SecondViewController.swift
//  Say2Me
//
//  Created by Tonismar Bernardo on 26/06/17.
//  Copyright © 2017 Tonismar Bernardo. All rights reserved.
//

import UIKit
import CloudSight

class LibraryViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CloudSightQueryDelegate
{

    let picker = UIImagePickerController()
    @IBOutlet weak var mImageViewLibrary: UIImageView!
    var csQuery: CloudSightQuery!
    var Description: String = ""
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        loading.isHidden = true
        loading.stopAnimating()
        
        //velho
        CloudSightConnection.sharedInstance().consumerKey = "jyDJg-ennUPhG9w2kG26Yw";
        CloudSightConnection.sharedInstance().consumerSecret = "BCwhNVbTDA_Mmc-7rLg0Og";
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        mImageViewLibrary.isUserInteractionEnabled = true
        mImageViewLibrary.addGestureRecognizer(tapGestureRecognizer)
    }
    
    //If voiceover is disabled when click over the image the description is dictated
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        _ = tapGestureRecognizer.view as! UIImageView
        
        if (UIAccessibilityIsVoiceOverRunning() == false) && (self.Description != "") {
            Util().sayIt(sentence: self.Description)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //load a photo from library
    @IBAction func loadFromLibrary(_ sender: UIButton) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.modalPresentationStyle = .popover
        present(picker, animated: true, completion: nil)
    }

    //put the image inside de image view
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        mImageViewLibrary.contentMode = .scaleAspectFit
        mImageViewLibrary.image = chosenImage
        
        dismiss(animated: true, completion: nil)
        
        //todo - call the webserve to load the picture
        csQuery = CloudSightQuery(image: UIImageJPEGRepresentation(chosenImage, 0.8), atLocation: CGPoint.zero, withDelegate: self, atPlacemark: nil, withDeviceId: "device-id")
        csQuery.start()
        
        if (!UIAccessibilityIsVoiceOverRunning()) {
            Util().sayIt(sentence: "Por favor, espere.")
        }
        
        loading.isHidden = false
        loading.startAnimating()
    }
    
    //Cancel button from library
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func cloudSightQueryDidFinishUploading(_ query: CloudSightQuery!) {
        print("cloudSighQueryDidFinishUploading")
    }
    
    func cloudSightQueryDidFinishIdentifying(_ query: CloudSightQuery!) {
        let group = DispatchGroup()
        
        print("cloudSighQueryDidFinishIdentifying")
        
        group.enter()
        
        //Find image description asyncrologic
        DispatchQueue.main.async {
            group.leave()
        }
        
        group.notify(queue: .main) { print(query.name())
            
            if (UIAccessibilityIsVoiceOverRunning()) {
                self.mImageViewLibrary.accessibilityHint = query.name()
                UIAccessibilityPostNotification (UIAccessibilityScreenChangedNotification, self.mImageViewLibrary)
                print(query.name())
            } else {
                Util().sayIt(sentence: query.name())
            }
            
            self.Description = query.name()
            self.loading.stopAnimating()
            self.loading.isHidden = true
        }
    }
    
    func cloudSightQueryDidFail(_ query: CloudSightQuery!, withError error: Error!) {
        print("CloudSight Failure: \(error)")
        Util().sayIt(sentence: "Não foi possível descrever a imagem.")
    }

    
}

