//
//  TakePictureViewController.swift
//  Say2Me
//
//  Created by Tonismar Bernardo on 26/06/17.
//  Copyright © 2017 Tonismar Bernardo. All rights reserved.
//

import UIKit
import CloudSight
import AVFoundation

class TakePictureViewController: UIViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate, CloudSightQueryDelegate{
    
    let picker = UIImagePickerController()
    @IBOutlet weak var mImageView: UIImageView!
    var csQuery: CloudSightQuery!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    var Description: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        loading.isHidden = true
        loading.stopAnimating()
        
        //velho
        CloudSightConnection.sharedInstance().consumerKey = "jyDJg-ennUPhG9w2kG26Yw";
        CloudSightConnection.sharedInstance().consumerSecret = "BCwhNVbTDA_Mmc-7rLg0Og";
        
        //novo
        //CloudSightConnection.sharedInstance().consumerKey = "BzQnHAHZ1HHqHZKCVP3H9w";
        //CloudSightConnection.sharedInstance().consumerSecret = "3tVROuPj0JVDg4sc5dTm6Q";
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        mImageView.isUserInteractionEnabled = true
        mImageView.addGestureRecognizer(tapGestureRecognizer)

    }
    
    //If voiceover is disabled when click over the image the description is dictated
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        _ = tapGestureRecognizer.view as! UIImageView
        
        if (UIAccessibilityIsVoiceOverRunning() == false) && (self.Description != "") {
            Util().sayIt(sentence: self.Description)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //call camera app and take a picture
    @IBAction func takePic(_ sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker, animated: true, completion: nil)
        } else {
            //noCamera()
            debugPrint("Erro");
        }
    }
    
    //put the image inside de image view
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        mImageView.contentMode = .scaleAspectFit
        mImageView.image = chosenImage
        
        dismiss(animated: true, completion: nil)
        
        //save image to library
        UIImageWriteToSavedPhotosAlbum(mImageView.image!, nil, nil, nil)
        
        //todo - call the webserve to load the picture
        csQuery = CloudSightQuery(image: UIImageJPEGRepresentation(chosenImage, 0.8), atLocation: CGPoint.zero, withDelegate: self, atPlacemark: nil, withDeviceId: "device-id")
        csQuery.start()
        
        if (!UIAccessibilityIsVoiceOverRunning()) {
            Util().sayIt(sentence: "Por favor, espere")
        }
        
        loading.isHidden = false
        loading.startAnimating()
    }

    //cancel button of camera
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    func cloudSightQueryDidFinishUploading(_ query: CloudSightQuery!) {
        print("cloudSightQueryDidFinishUploading")
    }
    
    func cloudSightQueryDidFinishIdentifying(_ query: CloudSightQuery!) {
        let group = DispatchGroup()
        
        print("cloudSightQueryDidFinishIdentifying")
        
        group.enter()

        //Find image description asyncrologic
        DispatchQueue.main.async {
            group.leave()
        }
        
        group.notify(queue: .main) {
            
            if (UIAccessibilityIsVoiceOverRunning()) {
                self.mImageView.accessibilityHint = query.name()
                UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.mImageView)
            } else {
                Util().sayIt(sentence: query.name())
            }
            
            self.Description = query.name()
            self.loading.stopAnimating()
            self.loading.isHidden = true
        }
    }
    
    func cloudSightQueryDidFail(_ query: CloudSightQuery!, withError error: Error!) {
        print("CloudSight Failure: \(error)")
        Util().sayIt(sentence: "Não foi possível descrever a imagem.")
    }
    
}

