//
//  Util.swift
//  Say2Me
//
//  Created by Tonismar Bernardo on 03/07/17.
//  Copyright © 2017 Tonismar Bernardo. All rights reserved.
//

import Foundation
import AVFoundation

class Util {

    //Set Text to Speech, passing text by parameter
    func sayIt(sentence: String) {
        var utt = AVSpeechUtterance(string: "")
        let synth = AVSpeechSynthesizer()
        
        if (sentence != "") {
            utt = AVSpeechUtterance(string: sentence)
            utt.rate = 0.4
            utt.pitchMultiplier = 1.2
            utt.voice = AVSpeechSynthesisVoice(language: "en-US")
            synth.speak(utt)
        }
        
    }
}
